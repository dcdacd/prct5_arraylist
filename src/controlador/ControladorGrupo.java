/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import modelo.Grupo;
import modelo.IModelo;
import vista.IVista;
import vista.Vista;

/**
 *
 * @author paco
 */
public class ControladorGrupo {

    private IModelo modelo;
    private IVista<Grupo> vistaGrupo;

    ControladorGrupo(IModelo modelo, IVista<Grupo> vistaGrupo) throws IOException {
        this.modelo = modelo;
        this.vistaGrupo = vistaGrupo;
        menuCrud();
    }

    private void menuCrud() throws IOException {
        String linea;
        Vista vista = new Vista();
        Grupo grupo = new Grupo();
        char opcion = ' ';
        String id; // Posicion a actualziar// Posicion a actualizar

        do {

            vista.mostrarTexto("\nGRUPO");
            opcion = vista.menuCrud();

            switch (opcion) {

                case 'e': // Exit
                    vista.exit();
                    break;

                case 'c': // Create
                    grupo = vistaGrupo.obtener();
                    modelo.create(grupo);
                    break;

                case 'r': // Read
                    vistaGrupo.mostrar(modelo.readg());
                    break;

                case 'u':  // Actualizar
                    id = vista.getId();
                    grupo = vistaGrupo.obtener();
                    grupo.setId(id);
                    modelo.update(grupo);
                    break;

                case 'd': // Borrar
                    id = vista.getId();
                    grupo.setId(id);
                    modelo.delete(grupo);
                    break;

                default:
                    vista.mostrarError("Opción Incorrecta");
                    break;
            }

        } while (opcion != 'e');

    }

}
