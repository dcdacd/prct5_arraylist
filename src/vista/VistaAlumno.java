/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Alumno;
import modelo.Grupo;
import static vista.Vista.validarEmail;

/**
 *
 * @author paco
 * @param <Alumno>
 */
public class VistaAlumno implements IVista<Alumno> {

    public Alumno obtener() {

        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

        Grupo grupo = new Grupo();
        Alumno alumno = new Alumno();
        String nombre = "";
        String email = "";
        String linea = "";
        int edad = 0;
        boolean error = true;
        boolean esvalido = true;
        String id;
        String idg = "";

        System.out.println("\nOBTENER ALUMNO");

        System.out.print("Nombre: ");
        try {
            nombre = br.readLine();
        } catch (IOException ex) {
            Logger.getLogger(Vista.class.getName()).log(Level.SEVERE, null, ex);
        }

        while (error == true) {

            System.out.print("Edad: ");
            try {
                linea = br.readLine();
                edad = Integer.parseInt(linea);
                error = false;
            } catch (IOException ex) {
                Logger.getLogger(Vista.class.getName()).log(Level.SEVERE, null, ex);

            } catch (NumberFormatException nfe) {
                System.out.println("Error: Se debe introducir un número");
                error = true;
            }

        } // while

        error = true;
        while (error == true) {

            System.out.print("Email: ");
            try {
                email = br.readLine();
                esvalido = validarEmail(email);
                if (esvalido) {
                    error = false;
                } else {
                    System.out.println("Error: Email no valido");
                    error = true;
                }
            } catch (IOException ex) {
                Logger.getLogger(Vista.class.getName()).log(Level.SEVERE, null, ex);

            }

        } // while

        System.out.print("Id Grupo : ");
        try {
            idg = br.readLine();
        } catch (IOException ex) {
            Logger.getLogger(Vista.class.getName()).log(Level.SEVERE, null, ex);
        }

        alumno.setNombre(nombre);
        alumno.setEdad(edad);
        alumno.setEmail(email);

        grupo.setId(idg);
        alumno.setGrupo(grupo);

        return alumno;
    }

    public void mostrar(HashSet hs) {
        Vista v = new Vista();
        Iterator it = hs.iterator();
        Alumno alumno = null;

        System.out.println("\nMOSTRAR ALUMNOS");
        while (it.hasNext()) {
            alumno = (Alumno) it.next();
            System.out.println(alumno);

        }
    }

}
