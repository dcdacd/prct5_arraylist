/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class ModeloArraylist implements IModelo {

   ArrayList alumnos = new ArrayList();
   ArrayList grupos = new ArrayList();

   int ida = 0;
   int idg = 0;

   @Override
   public void create(Alumno alumno) {
      alumno.setId("" + ida);
      alumnos.add(alumno);
      ida++;
   }

   public void update(Alumno alumno) {

      Iterator it = alumnos.iterator();
      Alumno a;
      int pos = 0;

      while (it.hasNext()) {
         a = (Alumno) it.next();
         if (a.getId().equals(alumno.getId())) {
            alumnos.remove(a);
            // alumnos.add(alumno);
            // No se puede borrar y añadir en el mismo iterador
         }
      }
      alumnos.add(alumno);

   }

   public void delete(Alumno alumno) {

      Iterator it = alumnos.iterator();
      Alumno a;
      int pos = 0;

      while (it.hasNext()) {
         a = (Alumno) it.next();
         if (a.getId().equals(alumno.getId())) {
            alumnos.remove(a);
            break;
         }
      }

   }

   @Override
   public void create(Grupo g) {
      g.setId("" + idg);
      grupos.add(g);
      idg++;
   }

   @Override
   public void update(Grupo grupo) {
      Iterator it = grupos.iterator();
      Grupo g;
      while (it.hasNext()) {
         g = (Grupo) it.next();
         if (g.getId().equals(grupo.getId())) {
            grupos.remove(g);
            break;
         }
      }
      grupos.add(grupo);
   }

   @Override
   public void delete(Grupo grupo) {
      Iterator it = grupos.iterator();
      Grupo g;
      int pos = 0;

      while (it.hasNext()) {
         g = (Grupo) it.next();
         if (g.getId().equals(grupo.getId())) {
            grupos.remove(g);
            break;
         }
      }
   }

   @Override
   public HashSet<Grupo> readg() {
      HashSet hs = new HashSet();
      Iterator it = grupos.iterator();
      while(it.hasNext()) {
          hs.add((Grupo) it.next());
      }
      return hs;
   }

   @Override
   public HashSet<Alumno> reada() {
      HashSet hs = new HashSet();
      Iterator it = alumnos.iterator();
      while(it.hasNext()) {
          hs.add((Alumno) it.next());
      }
      return hs;
   }

}